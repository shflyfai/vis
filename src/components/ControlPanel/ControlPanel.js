import React, { useState, useRef, useCallback } from "react";
import Papa from "papaparse";
import "./ControlPanel.css";
import { styled } from "@mui/styles";
import ShowTable from "../ControlPanel/Table";
import BarChart from "../Chart/Barchart";
import Modal from "react-modal";
import { FlyToInterpolator } from "deck.gl";
import { viewportState } from "../../utils";
import { useRecoilState } from "recoil";
import Layers from "../MapComponent/Layers";
import UploadButton from "../UploadFile/UploadFile";
import TocIcon from "@mui/icons-material/Toc";
import RemoveIcon from "@mui/icons-material/Remove";

//modal style
const customStyles = {
  overlay: {
    position: "fixed",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "rgba(255, 255, 255, 0.75)",
    zIndex: 2,
  },
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "80%",
    height: "80%",
  },
};

//remove icon style
const RemoveButton = styled(RemoveIcon)({
  "&.MuiSvgIcon-root": {
    width: 25,
    height: 25,
    color: "rgb(0, 71, 171)",
    position: "absolute",
    right: "27px",
    alignItems: "flex-end",
    "&:hover": {
      backgroundColor: "#f02c12",
      color: "white",
    },
  },
});

//show data icon style
const ShowDataButton = styled(TocIcon)({
  "&.MuiSvgIcon-root": {
    width: 25,
    height: 25,
    color: "rgb(0, 71, 171)",
    position: "absolute",
    right: "0",
    "&:hover": {
      backgroundColor: "rgb(0, 71, 171)",
      color: "white",
    },
  },
});

Modal.setAppElement(document.getElementById("root"));

function ControlPanel() {
  const [file, setFile] = useState("");
  const [list, setList] = useState([]);
  const [selected, setSelected] = useState([]);
  const [loading, setLoading] = useState(false);
  const [buttonloading, setButtonLoading] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isUpload, setIsUpload] = useState(true);
  const [isShow, setIsShow] = useState(false);
  const [isShowData, setIsShowData] = useState(false);
  const [isShowChart, setIsShowChart] = useState(false);
  const [viewport, setViewport] = useRecoilState(viewportState);

  const handleOnClick = () => {
    setIsShow(!isShow);
  };

  const inputRef = useRef(null);

  function openChart() {
    setIsShowChart(!isShowChart);
  }

  let subtitle;
  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = "#9a6fb0";
  }

  function closeModal() {
    setIsOpen(false);
  }

  //reset input value
  const resetFileInput = () => {
    inputRef.current.value = null;
    setList([]);
    setFile(null);
    setSelected([]);
    setIsShow(false);
  };

  //set file name
  const handleOnChange = (e) => {
    setFile(e.target.files[0]);
  };

  //function to extract csv data into each column
  //format : [{column_name: 'column_name', values: [array]}, .... ]
  const handleSubmit = (event) => {
    setList([]);
    setSelected([]);
    setButtonLoading(true);
    event.preventDefault();

    Papa.parse(file, {
      header: true,
      skipEmptyLines: true,
      complete: function (results) {
        const rowsArray = [];
        const valuesArray = [];

        // Iterating data to get column name and their values
        results.data.map((d) => {
          rowsArray.push(Object.keys(d));
          valuesArray.push(Object.values(d));
        });

        const headers = rowsArray[0];
        const refinedList = [];

        headers.forEach((header, index) => {
          // Get items from the rows at index of header..

          const values = [];

          valuesArray.forEach((row) => {
            values.push(row[index].trim());
          });

          refinedList.push({
            name: header,
            values: values,
          });
        });

        // console.log(refinedList);
        // Set state...

        setList(refinedList);
        setButtonLoading(false);
        setLoading(true);
        setIsShow(true);
      },
    });
  };

  //set selected attribute and format them to proper format
  //format : [{timestamp: timestamp, latitude: latitude, longitude: longitude}]
  const handleSelection = (event) => {
    event.preventDefault();
    const { x, y, z } = event.target.elements;

    const lat = list.find((el) => el.name === x.value);
    const lng = list.find((el) => el.name === y.value);
    const time = list.find((el) => el.name === z.value);

    let newTime = [];
    for (var i = 0; i < time.values.length; i++) {
      const d = Number(time.values[i]);
      if (isNaN(d)) {
        const t = new Date(time.values[i]);
        newTime.push(t.getTime());
      } else if (d > 1000000000000) {
        newTime.push(new Date(d).getTime());
      } else {
        newTime.push(new Date(d * 1000).getTime());
      }
    }

    const newLat = lat && lat.values.map((d) => Number(d));
    const newLng = lng && lng.values.map((d) => Number(d));

    for (var i = 0; i < newLat.length; i++) {
      if (isNaN(newLat[i])) {
        alert(
          "Can not read this data type (NaN) in Lattitude \nPlease check your file"
        );
        return [];
      }
    }

    for (var i = 0; i < newLng.length; i++) {
      if (isNaN(newLng[i])) {
        alert(
          "Can not read this data type (NaN) in Longitude \nPlease check your file"
        );
        return [];
      }
    }

    let copiedArray = [];
    for (var i = 0; i < newLng.length; i++) {
      copiedArray.push({
        latitude: newLat[i],
        longitude: newLng[i],
        timestamp: newTime[i],
      });
      setSelected(copiedArray);
    }


    const flyto = {
      longitude: newLng[0],
      latitude: newLat[0],
      zoom: 10,
      maxZoom: 15,
      pitch: 5,
      transitionDuration: 2500,
      transitionInterpolator: new FlyToInterpolator(),
    };

    setViewport(flyto);
    setIsUpload(false);
    setIsShowData(true);
  };

  //find latitude column
  let lat_res = list.filter(
    (code) => code.name.includes("lat") || code.name.includes("Lat")
  );

  //find longitude column
  let lng_res = list.filter(
    (code) =>
      code.name === "lng" ||
      code.name === "Lng" ||
      code.name.includes("Lon") ||
      code.name.includes("lon")
  );

  return (
    <>
      <div className="ControlPanel">
        {loading && (
          <button onClick={handleOnClick} className="expander">
            ✕
          </button>
        )}
        {/* form to upload CSV file */}
        <form>
          <div className="upload">
            <div className="container">
              {isUpload && (
                <>
                  <label>Upload File</label>
                  <UploadButton
                    inputref={inputRef}
                    onFileChange={handleOnChange}
                    onFormClick={handleSubmit}
                    buttonloading={buttonloading}
                  />
                </>
              )}
              {!isUpload && (
                <>
                  {file && (
                    <div className="file">
                      <h3>Current File: {file.name} </h3>
                      <ShowDataButton onClick={openModal} />
                      <RemoveButton onClick={resetFileInput} />
                    </div>
                  )}

                  <label>Upload new File</label>
                  <UploadButton
                    inputref={inputRef}
                    onFileChange={handleOnChange}
                    onFormClick={handleSubmit}
                    buttonloading={buttonloading}
                  />
                </>
              )}
            </div>
          </div>
        </form>

        {/* if CSV file has been uploaded then the selecting attribute form will show up */}
        {isShow && (
          <>
            {loading && (
              <>
                <form className="select-form" onSubmit={handleSelection}>
                  <div className="select">
                    <label htmlFor="latitude">Latitude</label>
                    <div className="custom-select">
                      <select id="latitude" name="x">
                        {lat_res.length > 0
                          ? lat_res.map((d) => (
                              <option key={d.name} value={d.name}>
                                {d.name}
                              </option>
                            ))
                          : list.map((option) => (
                              <option key={option.name} value={option.name}>
                                {option.name}
                              </option>
                            ))}
                      </select>
                    </div>
                  </div>

                  <div className="select">
                    <label htmlFor="longitude">Longitude</label>
                    <div className="custom-select">
                      <select id="longitude" name="y">
                        {lng_res.length > 0
                          ? lng_res.map((d) => (
                              <option key={d.name} value={d.name}>
                                {d.name}
                              </option>
                            ))
                          : list.map((option) => (
                              <option key={option.name} value={option.name}>
                                {option.name}
                              </option>
                            ))}
                      </select>
                    </div>
                  </div>

                  <div className="select">
                    <label htmlFor="Time Filter">Time</label>
                    <div className="custom-select">
                      <select name="z" id="Time Filter">
                        {list &&
                          list.map((option) => (
                            <option key={option.name} value={option.name}>
                              {option.name}
                            </option>
                          ))}
                      </select>
                    </div>
                  </div>
                  <button className="Apply" title="Submit" type="submit">
                    Apply
                  </button>
                  {isShowData && (
                    <button
                      className="Chart"
                      title="Chart"
                      type="button"
                      onClick={openChart}
                    >
                      Chart
                    </button>
                  )}
                </form>
              </>
            )}  
          </>
        )}
      </div>

      <Modal
        isOpen={isOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
      >
        <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Data</h2>
        <button className="Modal" onClick={closeModal}>
          ✕
        </button>
        {/* Table in a Modal dialog */}
        <ShowTable rows={selected} />
      </Modal>

      {/* display map and visualization layers on map */}
      <div className="button-wrapper">
        <Layers data={selected} viewstate={viewport} />
        {isShowChart && selected.length > 0 && <BarChart data={selected} />}
      </div>
    </>
  );
}

export default ControlPanel;
