import React from "react";
import LoadingButton from "@mui/lab/LoadingButton";
import "../ControlPanel/ControlPanel.css";
import { styled } from '@mui/styles';


function UploadButton({ onFileChange, buttonloading, onFormClick, inputref }) {
  const ProcessButton = styled(LoadingButton)({
    '&.MuiLoadingButton-root': {
      backgroundColor: "rgb(154, 110, 175,0.8);",
      color: "white",
      fontWeight: 500,
      textTransform: 'capitalize',
      border: "none",
      '&:hover, &:active': {
        backgroundColor: "rgb(154, 110, 175)",
      }
    }
  
  });

  return (
    <div className="fileUploadInput">

      <input
        ref={inputref}
        type="file"
        name="file"
        accept=".csv,.xlsx,.xls,.xml"
        onChange={onFileChange}
      />
      <ProcessButton
        onClick={onFormClick}
        loading={buttonloading}
        loadingIndicator="Loading…"
        variant="outlined"
      >
        Process
      </ProcessButton>
    </div>
  )
}


export default UploadButton;
